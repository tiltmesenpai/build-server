{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE DeriveGeneric     #-}
module Type where
  import Control.Concurrent
  import Control.Exception
  import Data.Typeable
  import Data.Maybe
  import GHC.Generics
  import GHC.IO.Handle
  import System.Exit
  import System.IO
  import System.Posix.Types

  import Data.UUID
  import Data.Aeson
  import Data.Aeson.Types (Pair)
  import Data.Text (pack)
  import Network.URI
  import System.Process
  import Data.HashTable.IO as HT

  data AppPausedException = AppPaused UUID deriving (Typeable, Show)

  instance Exception AppPausedException

  handleAppPaused :: BasicHashTable UUID AppState -> AppPausedException -> IO ()
  handleAppPaused ht (AppPaused app) = do
    maybeApp <- HT.lookup ht app
    case maybeApp of
      Nothing -> return ()
      Just appState -> do
        insert ht app appState { appHandle = Paused }
        case getProcess $ appHandle appState of
          Just process -> terminateProcess process
          Nothing      -> return ()

  data AppHandle = Init
                 | Download ProcessHandle
                 | Build    ProcessHandle
                 | Run      ProcessHandle
                 | Paused
                 | Dead     ExitCode

  getProcess :: AppHandle -> Maybe ProcessHandle
  getProcess (Download h) = Just h
  getProcess (Build    h) = Just h
  getProcess (Run      h) = Just h
  getProcess _            = Nothing

  instance Show AppHandle where
    show (Init)       = "Init"
    show (Download _) = "Download"
    show (Build    _) = "Build"
    show (Run      _) = "Run"
    show (Paused)     = "Paused"
    show (Dead     _) = "Dead"

  data AppState = App
    { appId         :: !UUID
    , appName       ::  String
    , appHandle     ::  AppHandle
    , appStdOut     ::  Handle
    , appStdErr     ::  Handle
    , appRemote     ::  URI
    , shouldRestart ::  Bool
    , appWatchdog   ::  ThreadId
    , appEnv        ::  [(String, String)]
    } deriving (Generic, Show)

  instance ToJSON AppState where
    toJSON App{..} = object
      [ "appId"      .= show appId
      , "appName"    .= appName
      , "appRemote"  .= show appRemote
      , "appStatus"  .= show appHandle
      , "appRestart" .= shouldRestart
      , "appEnv"     .= (object $ map packPair appEnv)
      ]

  packPair :: (String, String) -> Pair
  packPair (a, b) = (pack a) .= b

  data CreateApp = NewApp
    { newAppName    :: String
    , newAppRemote  :: URI
    , newAppRestart :: Bool
    , newAppStart   :: Bool
    , newAppEnv     :: [(String, String)]
    } deriving (Generic)
  
  instance FromJSON CreateApp where
    parseJSON (Object o) =  NewApp
                        <$> o .: "appName"
                        <*> (return . fromJust . parseURI =<< o .: "appRemote")
                        <*> o .: "appRestart"
                        <*> o .: "appStart"
                        <*> o .: "appEnv"
