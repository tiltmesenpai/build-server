{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns  #-}
{-# LANGUAGE RankNTypes      #-}
module Lib where
  import Control.Concurrent
  import Control.Exception
  import Control.Monad.Except
  import Control.Monad.Reader
  import Data.Maybe
  import GHC.IO.Handle
  import Servant
  import System.Exit
  import System.Environment
  import System.IO
  import System.Process
  import System.Process.Internals

  import Data.Aeson
  import Data.HashTable.IO as HT
  import Data.UUID
  import Data.UUID.V4

  import Type
  import Process 
  import Util

  type AppM = ReaderT (BasicHashTable UUID AppState) (ExceptT ServantErr IO)

  runAppM :: AppM a -> BasicHashTable UUID AppState -> IO a
  runAppM f s = do
    res <- runExceptT $ runReaderT f s
    return $ either throw id res

  runProc :: AppState -> CreateProcess -> IO (Handle, Handle, ProcessHandle)
  runProc App{..} proc = do
    env <- getEnvironment
    (_, Nothing, Nothing, handle)
      <- createProcess proc 
                       { cwd           = Just $ "/tmp/buildserver-" ++ show appId
                       , new_session   = True
                       , env           = Just $ env ++ appEnv
                       , std_out       = UseHandle stdout
                       , std_err       = UseHandle stderr
                       , delegate_ctlc = True
                       }
    return (stdout, stderr, handle)

  listApps :: AppM [AppState]
  listApps = map snd <$> (liftIO . toList =<< ask)

  listApp :: UUID -> AppM AppState
  listApp app = do
    hm <- ask
    maybeApp <- liftIO $ HT.lookup hm app
    case maybeApp of
      Just appState -> return appState
      Nothing       -> throwError err404

  updateApp :: UUID -> CreateApp -> AppM AppState
  updateApp id NewApp{..} = do
    hm <- ask
    pauseApp id
    Just appState <- liftIO $ HT.lookup hm id
    let app = appState {
        appName       = newAppName
      , appHandle     = Init
      , appRemote     = newAppRemote
      , shouldRestart = newAppRestart
      , appEnv        = newAppEnv
      }
    liftIO $! insert hm id app
    when newAppStart $ upgradeApp id
    return app

  makeApp :: CreateApp -> AppM AppState
  makeApp NewApp{..} = do
    hm <- ask
    id <- liftIO $ nextRandom
    thread <- liftIO $ myThreadId
    let app = App {
        appId         = id
      , appName       = newAppName
      , appHandle     = Init
      , appStdOut     = stdout
      , appStdErr     = stderr
      , appRemote     = newAppRemote
      , shouldRestart = newAppRestart
      , appEnv        = newAppEnv
      , appWatchdog   = thread
      }
    liftIO $! insert hm id app
    when newAppStart $ startApp id
    liftIO . print =<< liftIO . toList =<< ask
    return app

  startApp :: UUID -> AppM ()
  startApp app = do
    hm <- ask
    maybeApp <- liftIO $ HT.lookup hm app
    case maybeApp of
      Just appState -> do
        liftIO . putStrLn $ "Starting: " ++ (show $ appId appState)
        _ <- liftIO $ readCreateProcess (mkdir $ "/tmp/buildserver-" ++ show app) ""
        watchdog <- runApp app
        liftIO $! insert hm app appState { 
            appWatchdog = watchdog
          }
      Nothing -> throwError err404

  upgradeApp :: UUID -> AppM ()
  upgradeApp appId = pauseApp appId
    >> do
      hm <- ask
      Just app <- liftIO $ HT.lookup hm appId
      liftIO $ HT.insert hm appId =<< go app update Download
      runApp appId
      return ()
    where
      go :: AppState -> CreateProcess -> (ProcessHandle -> AppHandle) -> IO AppState
      go app proc const = do
        (stdout, stderr, handle) <- runProc app proc
        return app {
            appStdOut = stdout
          , appStdErr = stderr
          , appHandle = const handle
          }

  pauseApp :: UUID -> AppM ()
  pauseApp app = do
    maybeApp <- ask >>= \ht -> liftIO $ HT.lookup ht app
    case maybeApp of
      Just appState -> liftIO $ throwTo (appWatchdog appState) (AppPaused app)
      Nothing -> throwError err404

  statApp :: UUID -> AppM PidStat
  statApp app = do
    maybeApp <- ask >>= \ht -> liftIO $ HT.lookup ht app
    case maybeApp >>= getProcess . appHandle of
      Just handle -> do
        pidStat <- liftIO . withProcessHandle handle $ \process ->
          case process of
            OpenHandle pid -> Right <$> readStat pid
            ClosedHandle _ -> return $ Left err410
        case pidStat of
          Right stats -> return stats
          Left  err   -> throwError err
      Nothing -> throwError err404

  runApp :: UUID -> AppM ThreadId
  runApp app = do
      liftIO . putStrLn $ "Running: " ++ show app
      hm <- ask
      liftIO . forkIO $ loop hm app
    where
      loop ht appId = catch
        (do
          maybeApp <- HT.lookup ht appId
          case maybeApp of
            Just app@App{appHandle = Init}          -> stepApp ht appId >> loop ht appId
            Just app@App{appHandle = Download proc} -> step ht app proc
            Just app@App{appHandle = Build    proc} -> step ht app proc
            Just app@App{appHandle = Run      proc} -> step ht app proc
            _ -> putStrLn "No running process"
        )
        (handleAppPaused ht)
      step ht app process = do
        exitCode <- waitForProcess process
        putStrLn "Sepping app"
        case exitCode of
          ExitSuccess ->
            stepApp ht (appId app) >> loop ht (appId app)
          exitCode    ->
            insert ht (appId app) app{ appHandle = Dead exitCode }

  stepApp :: BasicHashTable UUID AppState -> UUID -> IO ()
  stepApp ht app = do
    putStrLn $ "Stepping: " ++ show app
    maybeApp <- HT.lookup ht app
    case maybeApp of
      Just a@App{appHandle, shouldRestart, appRemote} ->
        liftIO $!
          (HT.insert ht app =<< case appHandle of
            Init       -> go a (download appRemote) Download
            Download _ -> go a build Build
            Build    _ -> go a run   Run
            Run      _ -> if shouldRestart
              then go a run Run
              else return a { appHandle = Dead (ExitFailure 1) }
            Paused     -> return a
            Dead     _ -> return a
          ) >> (print =<< toList ht)
      Nothing -> return ()

    where
      go :: AppState -> CreateProcess -> (ProcessHandle -> AppHandle) -> IO AppState
      go app proc const = do
        (stdout, stderr, handle) <- runProc app proc
        return app {
            appStdOut = stdout
          , appStdErr = stderr
          , appHandle = const handle
          }
