module Main exposing (..)
import Navigation

import Model      exposing (..)
import View       exposing (..)
import Controller exposing (..)

main : Program Never Model Msg
main = Navigation.program Navigate
  { init   = init
  , update = update
  , subscriptions = subscriptions
  , view   = view
  }
